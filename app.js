
const io = require('socket.io')(3555);
const ss = require('socket.io-stream');

const fs = require('fs')

console.log('Server is starting....DONE');
io.on('connection', function (socket) {
    const recorder = require('node-record-lpcm16');
    const speech = require('@google-cloud/speech');

// Instantiates a client
   const client  = new speech.SpeechClient({
    keyFilename:'./vue-sst-05d71aaca65b.json'
   }
  );


    
// Create a recognize stream
const recognizeStream = client
.streamingRecognize(request)
.on('google error : ',function (error){
  console.log('hello');
  console.log(error);

})
.on('google data : ', data =>
  console.log(data)
);


var languageCode = 'en-US';
var sampleRateHertz = 48000;
var encoding = 'LINEAR16';
var config = {
    languageCode : languageCode,
    sampleRateHertz : sampleRateHertz,
    encoding : encoding
};
var request = {
  config : {
    languageCode : languageCode,
    sampleRateHertz : sampleRateHertz,
    encoding : encoding
},
  interimResults: false, //Get interim results from stream
};
console.log('SERVER CONNECT');
   ss(socket).on('START_SPEECH', function (stream) {
    console.log('started');




    recorder
    .record({
      sampleRateHertz: sampleRateHertz,
      threshold: 0,
      // Other options, see https://www.npmjs.com/package/node-record-lpcm16#options
      verbose: false,
      recordProgram: 'rec', // Try also "arecord" or "sox"
      silence: '10.0',
    })
    .stream()
    .on('error', console.error)
    .pipe(recognizeStream);
       //stream.pipe(recognizeStream);
   
    });

    
    socket.on('STOP_SPEECH', function () {
   
        console.log('Disconnected!');
    })


  console.log('Listening, press Ctrl+C to stop.');
    
});

